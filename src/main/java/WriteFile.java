import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;
import java.io.*;
import java.nio.charset.StandardCharsets;

public class WriteFile extends ActionTestScript{

	@Test
	public void testMain() throws IOException {

		final String filePath = getParameter(0).toString();
		final String value = getParameter(1).toString();
		final String methodParam = getParameter(2).toString();
		int method = Integer.parseInt(methodParam);

            File file = new File(filePath);
			try {
				file.createNewFile();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
                //PrintWriter writer;
                FileWriter writer = new FileWriter(file, true);
			try {
				switch (method) {
					case 1:
						writer.write(value+",");
						break;
					case 2:
						writer.write(value+"\n");
						break;
					default:
						writer.write("EMPTY\n");
						break;
				}
                //writer.write(value);
				// Flush et fermer le fichier
				writer.flush();
				writer.close();
			} catch (IOException e) {
				//throw new RuntimeException(e);
					e.printStackTrace();
			}
	}
}