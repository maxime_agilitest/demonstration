import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;
import java.util.Random;

public class TirageAleatoire extends ActionTestScript{

	/**
	 * Test Name : <b>TirageAleatoire</b>
	 * Test Author : <b>paulc</b>
	 * Test Description : <i></i>
	 * Test Prerequisites : <i></i>
	 */

	@Test
	public void testMain(){
		String csv = getParameter(0).toString();
		String[] elements = csv.split(",");
        Random random = new Random();
        int index = random.nextInt(elements.length);
        returnValues(elements[index]);
	}
}