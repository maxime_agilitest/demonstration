import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;

public class InterpretationMeteo extends ActionTestScript{

	/**
	 * Test Name : <b>InterpretationMeteo</b>
	 * Test Author : <b>LAPTOP-206PEVQR\maxim</b>
	 * Test Description : <i></i>
	 * Test Prerequisites : <i></i>
	 */

	@Test
	public void testMain(){
		// -----------------------------------------------
		// Get parameters passed by the calling script :
		// getParameter(int index)
		// -----------------------------------------------
		// String param0 = getParameter(0).toString();
		// int param0 = getParameter(0).toInt();
		// double param0 = getParameter(0).toDouble();
		// boolean param0 = getParameter(0).toBoolean();
		// -----------------------------------------------
		// int it = getIteration(); -> return current iteration loop
		// String path = getCsvFilePath(); -> return csv file path sent as parameter to call current script
		// File file = getCsvFile(); -> return csv file sent as parameter to call current script
		// File file = getAssetsFile("[relative path string]"); -> return a file in the project's 'assets' folder
		// String url = getAssetsUrl("[relative path string]"); -> return url path of a file in the project's 'assets' folder
		
		System.setProperty( "file.encoding", "UTF-8" );
		int param0 = getParameter(0).toInt();
		
		if(param0==0){
			returnValues("Temps clair et degage");
		} else if (param0==1){
			returnValues("Temps globalement clair et degage");
		}else if (param0==2){
			returnValues("Temps partiellement nuageux");
		}else if (param0==3){
			returnValues("Temps couvert");
		}else if (param0==45){
			returnValues("Risque de brouillard");
		}else if (param0==48){
			returnValues("Risque brouillard givrant");
		}else if (param0==51){
			returnValues("Risque de bruine legere");
		}else if (param0==53){
			returnValues("Risque de bruine moderee");
		}else if (param0==55){
			returnValues("Risque de bruine dense");
		}else if (param0==56){
			returnValues("Risque de bruine verglaçante legere");
		}else if (param0==57){
			returnValues("Risque de bruine verglaçante dense");
		}else if (param0==61){
			returnValues("Risque de pluie legere");
		}else if (param0==63){
			returnValues("Risque de pluie moderee");
		}else if (param0==65){
			returnValues("Risque de fortes pluies");
		}else if (param0==66){
			returnValues("Risque de pluie verglaçante");
		}else if (param0==67){
			returnValues("Risque de forte pluie verglaçante");
		}else if (param0==71){
			returnValues("Risque de chute de quelques flocons de neige");
		}else if (param0==73){
			returnValues("Risque de chute de neige");
		}else if (param0==75){
			returnValues("Risque de fortes chutes de neige");
		}else if (param0==77){
			returnValues("Risque de neige en grains");
		}else if (param0==80){
			returnValues("Risque d'averses legeres");
		}else if (param0==81){
			returnValues("Risque d'averses moderees");
		}else if (param0==82){
			returnValues("Risque de fortes averses");
		}else if (param0==85){
			returnValues("Risque de legeres averses de neige");
		}else if (param0==86){
			returnValues("Risque de fortes averses de neige");
		}else if (param0==95){
			returnValues("Risque d'orages");
		}else if (param0==96){
			returnValues("Risque d'orages avec de legeres averses de grele");
		}else if (param0==99){
			returnValues("Risque d'orages et de grele");
		}else{
			returnValues("Code meteo non pris en charge");
		}
		
		
		
		
		// -----------------------------------------------
		// Return string values to calling script :
		// returnValues(String ...)
		// -----------------------------------------------
		// returnValues("value", stringVariable);
	}
}