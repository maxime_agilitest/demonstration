CLS
@ECHO off

cd /D "%~dp0"

SET "ats_folder=%APPDATA%\ats"
SET "ats_tools=%ats_folder%\tools"

SET "clean_arg="
SET "suites_arg= -suiteXmlFiles="
SET "report_arg="
SET "valid_arg="

IF not exist %ats_folder% (mkdir %ats_folder%)
IF not exist %ats_tools% (mkdir %ats_tools%)

:continue
IF "%1"=="" GOTO end
IF %1 == clean (
	SET "clean_arg=clean"
	powershell -command "Remove-Item -recurse -Path \"%ats_tools%\jdk-base\""
)

IF %1 == valid (
	SET "valid_arg= -validationReport=1"
)

SET "test_xml=%1"

IF "%test_xml:~-4%"==".xml"	SET suites_arg=%suites_arg%%test_xml%,
IF "%test_xml:~0,12%"=="exec-report-" SET report_arg= -reportLevel=%test_xml:~-1%

SHIFT
GOTO continue
:end

SET java_exec=%ats_tools%\jdk-base\bin\java.exe
SET command_line=%clean_arg%%suites_arg:~0,-1%%report_arg%%valid_arg%

IF not exist %java_exec% (
	powershell -command "Invoke-WebRequest -Uri \"https://actiontestscript.com/tools/windows/jdk-base.zip\" -OutFile \"%ats_tools%\jdk-base.zip\""
	powershell -command "Expand-Archive \"%ats_tools%\jdk-base.zip\" -DestinationPath \"%ats_tools%\jdk-base\""
	powershell -command "Remove-Item -Path \"%ats_tools%\jdk-base.zip\""
)

IF not exist %java_exec% (
	SET java_exec="java"
)

%java_exec% AtsLauncher.java %command_line%